library(pacman)

p_load(tidyverse, openxlsx, data.table, car, janitor,
       caret, mlr, parallelMap, parallel, HDclassif, xgboost)



data <- fread("input/bank.csv")

# Age 
# data %>% names

data_1 <- data %>% 
  mutate_at(.vars = c("job", "marital", "education", 
                      "housing", "loan", "poutcome",
                      "deposit", "contact"), as.factor) %>%
  mutate(balance = balance - min(data$balance)) %>% 
  filter(balance > 2) %>% 
  dplyr::select(-c("default", "pdays", "day", "month"))

# Se debe eliminar un dato.


# nzv <- nearZeroVar(data, saveMetrics= TRUE)
# names(data)[nzv$nzv]
# Se excluyen las variables "default" y "pdays".
# Esto se debe a que son variables que presentan un fuerte desbalanceo en
# sus categorías, lo cual puede ser problemático, ya que en secciones posteriores
# se hará uso de métodos de remuestreo y Bootstrap.
# Se descarta la variable "contact" (tipo de contacto), debido a que
# no se cree que pueda ser útil dentro de los modelos a utilizar. Esto
# se evidenció con gráficos descriptivos, donde no se veía una relación 
# entre tener alguno de los dos créditos y las categorías de la variable. 

# No estoy seguro duration & campaign & previous
# table(data$housing, data$poutcome)
# stats::chisq.test(data$loan, data$deposit, correct = F)
# data$default %>% table
# data$pdays %>% table


data_X <- data_1 %>% dplyr::select(c("age", "job", "marital", "education", "balance",
                           "contact", "duration", "campaign",
                           "previous", "poutcome", "deposit")) %>% as.tibble()
preProcValues <- preProcess(data_X, method = c("center", "scale"))  



data_X_scales <- predict(preProcValues, data_X)
df_scales_y <- cbind(model.matrix(~.-1, data = data_X_scales), housing = factor(data_1$housing)) %>%
  as.data.frame() %>% mutate(housing = factor(housing))
names(df_scales_y) <- str_replace_all(names(df_scales_y), c("-" = "", "\\." = ""))
Task_scaled <- makeClassifTask(data = df_scales_y, target = "housing")

### KNN

knnParamSpace <- makeParamSet(makeDiscreteParam("k", values = 1:20))
gridSearch <- makeTuneControlGrid()
# cvForTuning <- makeResampleDesc("RepCV", folds = 10, reps = 20)
# tunedK <- tuneParams("classif.knn", task = diabetesTask,
#                      resampling = cvForTuning,
#                      par.set = knnParamSpace, control = gridSearch)

inner <- makeResampleDesc("CV", iters = 5, predict = "both")
outer <- makeResampleDesc("CV", iters = 5, predict = "both")

knnWrapper <- makeTuneWrapper("classif.knn", resampling = inner,
                              par.set = knnParamSpace,
                              control = gridSearch)

cvWithTuning <- resample(knnWrapper,
                         Task_scaled,
                         resampling = outer,
                         measures = list(acc, setAggregation(acc, train.mean)))


kFold <- makeResampleDesc(method = "CV", iters = 5,
                          stratify = TRUE, predict = "both")

logReg <- makeLearner("classif.logreg", predict.type = "prob")
# logRegModel <- train(logReg, titanicTask)

logRegModel <- resample(logReg, Task_scaled,
                        resampling = kFold,
                        measures = list(acc, setAggregation(acc, train.mean)))

lda <- makeLearner("classif.lda")
ldaModel <- train(lda, Task_scaled)

ldaCV <- resample(learner = lda, task = Task_scaled, resampling = kFold,
                  measures = list(acc, setAggregation(acc, train.mean)))

bayes <- makeLearner("classif.naiveBayes")

bayesCV <- resample(learner = bayes, task = Task_scaled,
                    resampling = kFold,
                    measures = list(acc, setAggregation(acc, train.mean)))

svm <- makeLearner("classif.svm")
kFold <- makeResampleDesc(method = "CV", iters = 3,
                          stratify = TRUE, predict = "both")
kernels <- c("polynomial", "radial", "sigmoid")
svmParamSpace <- makeParamSet(
  makeDiscreteParam("kernel", values = kernels),
  makeIntegerParam("degree", lower = 1, upper = 3),
  makeNumericParam("cost", lower = 0.1, upper = 10),
  makeNumericParam("gamma", lower = 0.1, 10))

randSearch <- makeTuneControlRandom(maxit = 10)

parallelStartSocket(cpus = detectCores())
tunedSvmPars <- tuneParams("classif.svm", task = Task_scaled,
                           resampling = kFold,
                           par.set = svmParamSpace,
                           control = randSearch,
                           measures = list(acc, setAggregation(acc, train.mean)))
parallelStop()



tree <- makeLearner("classif.rpart")

treeParamSpace <- makeParamSet(
  makeIntegerParam("minsplit", lower = 5, upper = 20),
  makeIntegerParam("minbucket", lower = 3, upper = 10),
  makeNumericParam("cp", lower = 0.01, upper = 0.1),
  makeIntegerParam("maxdepth", lower = 3, upper = 10))

randSearch <- makeTuneControlRandom(maxit = 200)
cvForTuning <- makeResampleDesc("CV", iters = 5)

parallelStartSocket(cpus = detectCores())
tunedTreePars <- tuneParams(tree, task = Task_scaled,
                            resampling = kFold,
                            par.set = treeParamSpace,
                            control = randSearch,
                            measures = list(acc, setAggregation(acc, train.mean)))
parallelStop()


forest <- makeLearner("classif.randomForest")
forestParamSpace <- makeParamSet(
  makeIntegerParam("ntree", lower = 300, upper = 700),
  makeIntegerParam("mtry", lower = 6, upper = 10),
  makeIntegerParam("nodesize", lower = 1, upper = 5),
  makeIntegerParam("maxnodes", lower = 5, upper = 20))
randSearch <- makeTuneControlRandom(maxit = 100)
cvForTuning <- makeResampleDesc("CV", iters = 5)
parallelStartSocket(cpus = detectCores())
tunedForestPars <- tuneParams(forest, task = Task_scaled,
                              resampling = kFold,
                              par.set = forestParamSpace,
                              control = randSearch,
                              measures = list(acc, setAggregation(acc, train.mean)))
parallelStop()
tunedForestPars


xgbParamSpace <- makeParamSet(
  makeNumericParam("eta", lower = 0, upper = 1),
  makeNumericParam("gamma", lower = 0, upper = 5),
  makeIntegerParam("max_depth", lower = 1, upper = 3),
  makeNumericParam("min_child_weight", lower = 1, upper = 10),
  makeNumericParam("subsample", lower = 0.5, upper = 1),
  makeNumericParam("colsample_bytree", lower = 0.5, upper = 1),
  makeIntegerParam("nrounds", lower = 20, upper = 20))
randSearch <- makeTuneControlRandom(maxit = 200)
xgb <- makeLearner("classif.xgboost")

tunedXgbPars <- tuneParams(xgb, task = Task_scaled,
                           resampling = kFold,
                           par.set = xgbParamSpace,
                           control = randSearch,
                           measures = list(acc, setAggregation(acc, train.mean)))

data_1 %>% names
data_to_model_1 <- data_1[, -c("loan")]
data_to_model_1 <- as.tibble(data_to_model_1)
data_to_model_2 <- data_1[, -c("housing")]
data_to_model_2 <- as.tibble(data_to_model_2)
# nums <- unlist(lapply(data_X, is.numeric), use.names = FALSE)  

Task <- makeClassifTask(data = data_to_model_1, target = "housing")
xgb <- makeLearner("classif.xgboost")

data_to_model_1_xgb <- mutate_at(data_to_model_1, 
                    .vars = vars(-housing), .funs = as.numeric)
xgbTask <- makeClassifTask(data = data_to_model_1_xgb,
                           target = "housing")

xgbParamSpace <- makeParamSet(
  makeNumericParam("eta", lower = 0, upper = 1),
  makeNumericParam("gamma", lower = 0, upper = 5),
  makeIntegerParam("max_depth", lower = 1, upper = 5),
  makeNumericParam("min_child_weight", lower = 1, upper = 10),
  makeNumericParam("subsample", lower = 0.5, upper = 1),
  makeNumericParam("colsample_bytree", lower = 0.5, upper = 1),
  makeIntegerParam("nrounds", lower = 20, upper = 20),
  makeDiscreteParam("eval_metric", values = c("merror", "mlogloss")))

randSearch <- makeTuneControlRandom(maxit = 2)
cvForTuning <- makeResampleDesc("CV", iters = 5)
tunedXgbPars <- tuneParams(xgb, task = xgbTask,
                           resampling = cvForTuning,
                           par.set = xgbParamSpace,
                           control = randSearch)

forest <- makeLearner("classif.randomForest")
forestParamSpace <- makeParamSet(
  makeIntegerParam("ntree", lower = 300, upper = 700),
  makeIntegerParam("mtry", lower = 6, upper = 10),
  makeIntegerParam("nodesize", lower = 1, upper = 5),
  makeIntegerParam("maxnodes", lower = 5, upper = 20))

randSearch <- makeTuneControlRandom(maxit = 100)
cvForTuning <- makeResampleDesc("CV", iters = 5)
parallelStartSocket(cpus = detectCores())
tunedForestPars <- tuneParams(forest, task = Task,
                              resampling = cvForTuning,
                              par.set = forestParamSpace,
                              control = randSearch,
                              measures = list(acc, fpr, fnr))

parallelStop()


outer <- makeResampleDesc("CV", iters = 2, predict = "both")
forestWrapper <- makeTuneWrapper("classif.randomForest",
                                 resampling = cvForTuning,
                                 par.set = forestParamSpace,
                                 control = randSearch)
parallelStartSocket(cpus = detectCores())
cvWithTuning <- resample(forestWrapper, Task, resampling = outer,
                         measures = list(acc, setAggregation(acc, train.mean)))
parallelStop()
cvWithTuning

cvWithTuning %>% names
cvWithTuning$measures.train
cvWithTuning$measures.test


randSearch <- makeTuneControlRandom(maxit = 100)
cvForTuning <- makeResampleDesc("CV", iters = 5)
parallelStartSocket(cpus = detectCores())
tunedForestPars <- tuneParams(forest, task = zooTask,
                              resampling = cvForTuning,
                              par.set = forestParamSpace,
                              control = randSearch)

randSearch <- makeTuneControlRandom(maxit = 100)
cvForTuning <- makeResampleDesc("CV", iters = 5)
Task <- makeClassifTask(data = data_to_model_1, target = "housing")
logReg <- makeLearner("classif.logreg", predict.type = "prob")
logRegModel <- train(logReg, Task)
kFold <- makeResampleDesc(method = "RepCV", folds = 10, reps = 50,
                          stratify = TRUE)
logRegWrapper <- makeImputeWrapper("classif.logreg")
logRegwithImpute <- mlr::resample(logReg, Task,
                             resampling = kFold, 
                             measures = list(acc, fpr, fnr))


kFold <- makeResampleDesc(method = "RepCV", folds = 10, reps = 50,
                          stratify = TRUE)
logRegModelData <- getLearnerModel(logRegModel)
coef(logRegModelData)



svm <- makeLearner("classif.svm")
getParamSet("classif.svm")

kernels <- c("polynomial", "radial", "sigmoid")
svmParamSpace <- makeParamSet(
  makeDiscreteParam("kernel", values = kernels),
  makeIntegerParam("degree", lower = 1, upper = 3),
  makeNumericParam("cost", lower = 0.1, upper = 10),
  makeNumericParam("gamma", lower = 0.1, 10))
randSearch <- makeTuneControlRandom(maxit = 20)
cvForTuning <- makeResampleDesc("RepCV", folds = 10, reps = 10)

# parallelStartSocket(cpus = detectCores())
# 
# tunedSvmPars <- tuneParams("classif.svm", task = Task,
#                            resampling = cvForTuning,
#                            par.set = svmParamSpace,
#                            control = randSearch)
# parallelStop()
# 
# tunedSvm <- setHyperPars(makeLearner("classif.svm"),
#                          par.vals = tunedSvmPars$x)
# tunedSvmModel <- train(tunedSvm, Task)


outer <- makeResampleDesc("CV", iters = 3)
svmWrapper <- makeTuneWrapper("classif.svm", resampling = cvForTuning,
                              par.set = svmParamSpace,
                              control = randSearch)
parallelStartSocket(cpus = detectCores())
cvWithTuning <- mlr::resample(svmWrapper, Task, resampling = outer)
parallelStop()

tunedForestPars <- tuneParams(forest, task = zooTask,
                              resampling = cvForTuning,
                              par.set = forestParamSpace,
                              control = randSearch)

# ?parallelStartSocket(cpus = detectCores())
normalizeFeatures(data_X)
# cor(data_X %>% dplyr::select(names(data_X)[nums]))


preProcValues <- preProcess(data_X, method = c("center", "scale"))




data$default %>% table
table(data$housing, data$loan)

ggplot(data) + 
  geom_bar(aes(housing), position = "dodge") +
  scale_fill_brewer(palette = "Set1")

ggplot(data) + 
  geom_bar(aes(loan), position = "dodge") +
  scale_fill_brewer(palette = "Set1")



ggplot(data) + 
  geom_bar(aes(loan, fill = poutcome), position = "dodge") +
  scale_fill_brewer(palette = "Set1")

summary(glm(loan ~ log(previous), data = data, family = binomial))
ggplot(data) + geom_boxplot(aes(x = housing, y = log(poutcome)))

ggplot(data) + geom_boxplot(aes(x = loan, y = age))

ggplot(data) + 
  geom_bar(aes(housing, fill = job), position = "dodge") +
  scale_fill_brewer(palette = "Set1")


ggplot(data) + 
  geom_bar(aes(housing, fill = marital), position = "dodge") +
  scale_fill_brewer(palette = "Set1")


ggplot(data) + 
  geom_bar(aes(loan, fill = marital), position = "dodge") +
  scale_fill_brewer(palette = "Set1")
