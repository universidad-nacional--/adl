---
title: "Prueba de Ingreso - Científico de Datos Semisenior Core"
subtitle: Prueba Técnica
author: 
- Camilo Avellaneda^[ [caavellanedag@unal.edu.co](caavellanedag@unal.edu.co) ]
date: "08 de Agosto de 2022"
site: "bookdown::bookdown_site"
lang: es-CO
fig_width: 4
fig_height: 2.5
fontsize: 12pt
header-includes:
   - \usepackage{amsmath}
   - \usepackage{graphics}
   - \usepackage[inline]{enumitem}
   - \usepackage{amsbsy}
   - \usepackage{multirow}
   - \usepackage{mathtools}
   - \usepackage{cancel}
   - \usepackage{url}
   - \usepackage{tikz}
   - \usepackage{float}
   - \usepackage{epstopdf}
   - \usepackage{enumitem}
   - \usepackage{bm}
   - \usepackage{color, colortbl}
   - \usepackage{natbib}
   - \usepackage{hyperref}
   - \usepackage{booktabs}
   - \usepackage{float}
   - \usepackage{fancyhdr}
output:
  bookdown::pdf_document2:
    citation_package: natbib
bibliography: references.bib
urlcolor: blue
---

\addtolength{\headheight}{1.0cm} <!--% make more space for the header-->
\pagestyle{fancyplain} <!--use fancy for all pages except chapter start-->



# Introducción

En este documento se plantea una propuesta con el fin de encontrar la oferta ideal para clientes existentes de una entidad bancaria. La información con la que se trabaja corresponde a aperturas históricas de los clientes en seis meses, incluyendo características propias de los usuarios. A partir de este conjunto de datos se buscar encontrar un modelo que permita proporcionar a la entidad el mejor producto a ofertar, de acuerdo a las características de cada cliente. 

Como variables objetivo se busca modelar la tenencia de créditos de vivienda ("housing") o un crédito de libre inversión ("loan"). Por otro lado, las variables independientes o explicativas son edad, tipo de trabajo, estado civil, nivel de educación, saldo bancario, entre otras. Es importante tener en cuenta que en el formato compartido, donde se describen las variables se tienen en cuenta variables de tenencia de diferentes productos, ingresos, egresos y saldos, las cuales no se incluyeron dentro de la base de datos. Por este motivo no fueron incluidas dentro de los modelos y respectivos resultados.

Dado que el objetivo principal es generar la recomendación de uno de los dos productos descritos en las variables objetivo y la tenencia de estos productos no corresponden a eventos mutuamente excluyentes se procede de la siguiente manera:

* Se realiza la depuración, ajustes y transformaciones necesarias en las variables independientes. En esta parte se incluye la exclusión de algunas variables que no se consideran útiles o pueden generar conflictos en los procesos de estimación y evaluación.

* Se realiza el ajuste, optimización de hiperparámetros y respectiva evaluación de los mismos para diferentes modelos descritos en la sección número dos. 

* Para cada uno de los modelos se determina una combinación de hiperparámetros que maximice la precisión o "accuracy".

* Una vez se tenga el modelo final para cada caso, se realiza la respectiva comparación entre los mismos, con el fin de determinar un modelo "ganador".

* De esta forma, tanto para los créditos de vivienda como los créditos de libre inversión, se determina un modelo que servirá para realizar la predicción de la probabilidad que un usuario especifico adquiera el producto. A cada usuario se le asignará el producto que mayor probabilidad tenga a partir de los modelos de cada categoría.

En la sección número dos se presenta una descripción general de la metodología tenida en cuenta dentro del ejercicio, seguido de la sección tres donde se encuentran los resultados y por último una serie de conclusiones. Por otro lado, se incluye un anexo con gráficos y análisis adicionales que fueron tenidos en cuenta. Toda la programación se comparte como anexo a este documento y el proyecto general puede ser consultado [aquí](https://gitlab.com/universidad-nacional--/adl). 

# Metodología utilizada




La base consta de 11162 registros, donde cada uno denota un cliente. Para cada caso se tiene información de los productos adquiridos (vivienda o libre inversión), además de las variables adicionales que en adelante servirán como explicativas. Estas  requieren un procesamiento adicional, ya que características como nivel educativo o estado civil deben ser transformadas en un factor. Adicionalmente, de acuerdo a la distribución de valores presentada por la variable de saldo en el sistema bancario (en la base denotada como "balance"), se decide aplicar un logaritmo natural sobre la misma, para disminuir el impacto que puede tener valores muy altos de esta variable. Para este paso se debe garantizar que la variable sea no negativa, por el dominio de la función logaritmo. 

Por otro lado, se excluyen las variables "default" y "pdays". Esto se debe a que son variables que presentan un fuerte desbalanceo en sus categorías, lo cual puede ser problemático, ya que en secciones posteriores se hará uso de métodos de remuestreo y Bootstrap. Se descarta la variable "contact" (tipo de contacto), debido a que no se cree que pueda ser útil dentro de los modelos a utilizar. Esto se evidenció con gráficos descriptivos y pruebas chi cuadrado sobre tablas de contingencia, donde no se veía una relación entre tener alguno de los dos créditos y las categorías de la variables mencionadas. De manera adicional la variable de días no fue tenida en cuenta, dado que se consideró que no contribuiría en los modelos realizados.

La totalidad del trabajo realizado se hizo en el software R (\cite{R}) y los modelos, con sus respectivos ajustes en la estimación y evaluación se realizaron mediante los paquetes mlr y caret (\cite{mlr} y \cite{caret}, respectivamente). 

Como se mencionó previamente, dado que se tienen dos productos a ofertar y los eventos de su tenencia no son mutuamente excluyentes, se consideran modelos por separado, de tal forma que los modelos "ganadores" para cada producto son tenidos en cuenta e integrados al final para determinar cuál es el producto más propenso a ser adquirido. De esta forma, los modelos deben realizar tareas de clasificación de los usuarios para asignar una variable binaria, que representa la tenencia o ausencia del producto. Por este motivo, los modelos considerados, junto con su respectiva descripción, son los siguientes:

* $K$ vecinos más cercanos (KNN): Es un algoritmo que busca cotejar cada observación con respecto a los registros más próximos en términos de una distancia, la cual generalmente se toma como la distancia euclidiana. La predicción se realiza mediante la característica modal observada en los vecinos más cercanos. Se caracteriza por ser un algoritmo simple en su estimación y costoso computacionalmente en la predicción.

* Modelo logístico (Logit): Es un modelo similar a los modelos de regresión lineal múltiple, con la diferencia que este busca explicar la variabilidad de una variable dicotomica. En su estructura se relaciona lo que se denomina como el "odds", que representa el cociente entre la probabilidad de éxito versus la probabilidad de fracaso, con una ecuación lineal mediante la función logaritmo.

* Análisis linear discriminante (LDA): Este algoritmo asume que las poblaciones correspondientes a las agrupaciones se distribuyen normal y a partir de esto busca encontrar una combinación lineal que maximice la distancia entre las mismas.

* Maquinas de soporte vectorial (MSV): Busca una serie de vectores que permiten separar las poblaciones, sin asumir normalidad en su distribución.

* Random forest (RF): Es un algoritmo basado en arboles de decisión. El enfoque es crear una cantidad previamente determinada de árboles de decisión con base en una submuestra tanto de registros como de variables, de tal forma que sean sencillos de estimar. Estas multiples clasificaciones se combinan para obtener una predicción s precisa, que es una tecnica de ensamblaje conocida como Bagging, la cual busca disminuir la varianza de los estimadores.

* XGBoost: Combina el poder predictivo de varios modelos, que se estiman secuencialmente con el fin de reducir gradualmente los errores de prediccion en cada iteración.

* Redes neuronales o perceptrones multicapa (PMC): Este algoritmo utiliza una serie de capas, que se calculan mediante la combinación lineal de capas anteriores y de funciones de activación que permiten incluir factores no lineales dentro de las predicciones a realizar. Se caracterizan por tener un número elevado de parámetros a estimar, lo cual se realiza mediante un algoritmo denominado como el descenso del gradiente.

Dado que es un ejercicio de clasificación, se consideró principalmente la precisión como criterio para la selección del mejor modelo. De manera adicional, dado que hay modelos que requieren la evaluación de hiperparámetros, esta se realizó mediante validación cruzada, de tal forma que se obtuvieron los valores más adecuados para cada caso. Los valores obtenidos para estos hiperparámetros se encuentran en los anexos. Una vez se obtuvieron los valores óptimos, se procede a realizar el mismo procedimiento de validación cruzada (CV) para obtener una estimación del porcentaje de precisión correspondiente a cada modelo. La idea detrás de la metodología de CV es calcular el error con base en registros no utilizados dentro del entrenamiento de los modelos.

A continuación se presentan los resultados correspondientes a los modelos evaluados para cada producto.

## Crédito de libre inversión




De manera inicial, la siguiente figura muestra la distribución de la variable relacionada con la adquisición del crédito de libre inversión. Allí se observa un claro desbalanceo en esta variable, lo cual puede influir dentro de la precisión si no se realiza un proceso de ajuste. En la literatura se encuentran diferentes soluciones para esta problemática, sin embargo acá solamente mencionamos la que se utilizó. Esta corresponde a la selección de manera aleatoria sobre el grupo que tiene la parte mayoritaria, de forma que al final del proceso ambas clases tienen una cantidad similar de registros. De esta forma, el modelo a realizar se basa en un conjunto de datos con 2918  registros, donde a cada categoría le corresponde la misma cantidad de filas.
![](DOCUMENTO_PRUEBA_CAMILO_AVELLANEDA_files/figure-latex/loan-1.pdf)<!-- --> 

En el cuadro que se muestra a continuación se muestra la precisión de cada uno de los modelos considerados una vez se realiza el proceso de optimización de los hiperparámetros y de estimación de los parámetros que son requeridos. Allí se observa que el modelo de mayor ajuste sobre la muestra de entrenamiento es la red neuronal o perceptrón multicapa (PMC), sin embargo es evidente que hay un sobre ajuste, ya que el porcentaje sobre la muestra de prueba es menor. De esta forma, el siguiente modelo con mejores atributos es el XGB, el cual se determina como el que mejor rendimiento tiene para este caso, considerando sus valores tanto en la muestra de entrenamiento como en la de prueba. 



\begin{longtable}[t]{lll}
\toprule
acc\_test\_mean & acc\_train\_mean & modelo\\
\midrule
\endfirsthead
\multicolumn{3}{@{}l}{\textit{(continued)}}\\
\toprule
acc\_test\_mean & acc\_train\_mean & modelo\\
\midrule
\endhead

\endfoot
\bottomrule
\endlastfoot
\cellcolor{gray!6}{59.0\%} & \cellcolor{gray!6}{59.0\%} & \cellcolor{gray!6}{Bayes}\\
63.0\% & 63.0\% & LDA\\
\cellcolor{gray!6}{63.0\%} & \cellcolor{gray!6}{64.0\%} & \cellcolor{gray!6}{Logit}\\
59.0\% & 66.0\% & KNN\\
\cellcolor{gray!6}{63.0\%} & \cellcolor{gray!6}{64.0\%} & \cellcolor{gray!6}{MSV}\\
\addlinespace
59.0\% & 62.0\% & Rpart\\
\cellcolor{gray!6}{64.0\%} & \cellcolor{gray!6}{65.0\%} & \cellcolor{gray!6}{RF}\\
64.0\% & 67.0\% & XGB\\
\cellcolor{gray!6}{60.0\%} & \cellcolor{gray!6}{76.0\%} & \cellcolor{gray!6}{PMC}\\*
\end{longtable}


## Crédito de vivienda

 De manera análoga a cómo se procedió con el crédito de libre inversión, la figura de enseguida muestra la distribución de acuerdo a la variable que representa la adquisición del crédito de vivienda. Allí se observa que las categorías se distribuyen en términos similares, desde una perspectiva porcentual, por lo cual no se requieren procesos de remuestreo o de selección como en el caso anterior. 
 
 
 
![](DOCUMENTO_PRUEBA_CAMILO_AVELLANEDA_files/figure-latex/housing-1.pdf)<!-- --> 

Por otro lado, se procede a la estimación y respectiva evaluación de los modelos correspondientes al crédito de vivienda. La tabla que se presenta enseguida muestran las precisiones respectivas para las muestras de entrenamiento como para las muestras de prueba. Es importante tener en cuenta que los valores que aquí se presentan son los promedios sobre las validaciones cruzadas elaboradas. A partir de los resultados que se presentan en esta tabla se resalta el hecho de que el modelo de mejor rendimiento, nuevamente es el XGB, sin embargo al igual se observa un rendimiento similar en modelos como el KNN, RF y PMC. 


\begin{longtable}[t]{lll}
\toprule
acc\_test\_mean & acc\_train\_mean & modelo\\
\midrule
\endfirsthead
\multicolumn{3}{@{}l}{\textit{(continued)}}\\
\toprule
acc\_test\_mean & acc\_train\_mean & modelo\\
\midrule
\endhead

\endfoot
\bottomrule
\endlastfoot
\cellcolor{gray!6}{61.0\%} & \cellcolor{gray!6}{61.0\%} & \cellcolor{gray!6}{Bayes}\\
69.0\% & 69.0\% & LDA\\
\cellcolor{gray!6}{69.0\%} & \cellcolor{gray!6}{69.0\%} & \cellcolor{gray!6}{Logit}\\
67.0\% & 71.0\% & KNN\\
\cellcolor{gray!6}{66.0\%} & \cellcolor{gray!6}{67.0\%} & \cellcolor{gray!6}{Rpart}\\
\addlinespace
68.0\% & 69.0\% & RF\\
\cellcolor{gray!6}{71.0\%} & \cellcolor{gray!6}{72.0\%} & \cellcolor{gray!6}{XGB}\\
69.0\% & 68.0\% & PMC\\*
\end{longtable}

Por los motivos expuestos previamente, los modelos de mejor rendimiento en ambos casos, tanto para el crédito de libre inversión como para el crédito de vivienda es el XGB. A partir de los mismos se estiman las probabilidades de que cada usuario adquiera uno de los productos y de esta manera, se asignará el mejor producto de acuerdo a la probabilidad que sea más alta.


# Conclusiones

* En este documento se elaboró una metodología que permite realizar la selección del mejor producto a ofertar dadas las características de los usuarios. Esta propuesta se trabajó con dos productos unicamente, sin embargo puede generalizarse a más categorías, donde de manera muy análoga se consideran modelos adicionales y para cada usuario se escoje la categoría que tenga una mayor probabilidad asignada. En caso de que se desee realizar la recomendación de más de un producto pueden tomarse los productos con mayor probabilidades predichas a partir de los modelos de aprendizaje considerados.

*  En este caso, a partir de la información, se concluye que los modelos más adecuados de acuerdo a la métrica de precisión son los XGB tanto para el crédito de libre inversión, como para el crédito de vivienda. 

* Para mejorar el rendimiento de los modelos considerados es importante tener más información, ya sea mediante el acceso a más meses o más variables como las que se indican en el formato de descripción de la información. 

* Es importante resaltar que por ejemplo en el caso de crédito de vivienda, modelos como el KNN o el Logit presentan rendimientos similares al XGB, lo cual puede considerarse como una ventaja, de acuerdo a los objetivos del ejercicio, debido a la simplicidad del KNN y a la interpretabilidad de los coeficientes del modelo Logit.

* Debido a que los procesos que se han presentado dependen de la selección aleatoria de registros y características, los resultados, dependiendo de la ejecución, pueden diferir.

* Es evidente que el mejor modelo para cada escenario es diferente dependiendo de la naturaleza del conjunto de datos, no se puede asumir siempre el mismo. Es por esta razon que se validaron algunas de las técnicas  más conocidas, para seleccionar el mejor modelo.

# Anexo

A continuación se presentan los gráficos descriptivos elaborados, con el fin de obtener ideas acerca de la relación entre las diferentes variables tenidas en cuenta dentro del ejercicio.

![](DOCUMENTO_PRUEBA_CAMILO_AVELLANEDA_files/figure-latex/unnamed-chunk-2-1.pdf)<!-- --> ![](DOCUMENTO_PRUEBA_CAMILO_AVELLANEDA_files/figure-latex/unnamed-chunk-2-2.pdf)<!-- --> ![](DOCUMENTO_PRUEBA_CAMILO_AVELLANEDA_files/figure-latex/unnamed-chunk-2-3.pdf)<!-- --> ![](DOCUMENTO_PRUEBA_CAMILO_AVELLANEDA_files/figure-latex/unnamed-chunk-2-4.pdf)<!-- --> ![](DOCUMENTO_PRUEBA_CAMILO_AVELLANEDA_files/figure-latex/unnamed-chunk-2-5.pdf)<!-- --> 

